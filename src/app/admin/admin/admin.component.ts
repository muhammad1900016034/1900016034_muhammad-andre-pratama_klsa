import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  items : any;
  mode:string='side';
  constructor(
    public api: ApiService,
    public router: Router
  ) { }

  ngOnInit(): void {
    this.checkLogin();
    this.menu();
  }
  
  checkLogin()
  {
    this.api.get('bookswithauth/status').subscribe(result=>{
      //is logged in
      return;
    }, error=>{
      //not logged in
      this.router.navigate(['/login']);
    })
  }
  logout()
  {
    let conf=confirm('keluar aplikasi?');
    if(conf)
    {
      localStorage.removeItem('appToken');
      window.location.reload();
    }
  }
  menu(){
    this.items=[
      {icon:'dashboard', name:'Dasboard', url:'/admin/dashboard'},
      {icon:'camera_enhance', name:'Product', url:'/admin/produk'} 
    ]
  }

}
